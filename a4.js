function fibAdaption(n){
	
	var fib = [];
	fib.push(1);
	fib.push(1);
	fib.push(1);
	for(i=3; i<=n; i++)
	{
	    fib.push(fib[i-2] + fib[i-3]);
	}
	return fib[fib.length-1];

}

console.log(fibAdaption(150));