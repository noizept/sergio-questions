
/// not 100% sure but

/// The reason why the for is always 5, is because of Jscript Async nature (just like NodeJs) ( i might be wrong)
//  when the timeout waits for its callback async way, the for loop has already finished
// therefore the last element is always shown

// another reason might be because we are not passing a value but rather a reference of the " i " variable
// and by the time the callback function of setimeout is over, the reference pointing to " i " gets its value
// that is 5 by the end of the for execution 




// ORiginal
 //                       for (var i = 1; i < 5; i++) {
   //                         setTimeout(function () {
    //                                        console.info(i);
      //                      }, 0);
        //                }




///Possible solution passing a function with the value of "i" in that given moment
function timeout(i) {
  setTimeout(function() { console.log(i) }, 0);
}

for (var i = 1; i <= 5; ++i)
  timeout(i);



