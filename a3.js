
// Object of strings containing the associated number of the month according to JScript definition
var months={
        'January' : '00',
        'February' : '01',
        'March' : '02',
        'April' : '03',
        'May' : '04',
        'June' : '05',
        'July' : '06',
        'August' : '07',
        'September' : '08',
        'October' : '09',
        'November' : '10',
        'December' : '11',
}

// Parse the string into two individual sets of Month Year
// Convert the strings of "Month XXXX" to a corresponding Date OBject
// Iterate over each month to find out if there are 5 thursdays
function getNumberOfMonths(aDateRange)
{

    //Find date matches 
    var result=aDateRange.match(/\w+ [0-9]{4}/g);
  
    // Dates Parsing
    var startDate=result[0].split(' ');
    var endDate=result[1].split(' ');

    //Counter of how many months contain 5 thursdays  
    var counterMonths=0;

    //Parsing string to Date OObject
    var initialDate=new Date();
    initialDate.setYear(startDate[1]);
    initialDate.setMonth(months[startDate[0]]);
    
    //Parsing string to Date OObject
    var finishDate=new Date();
    finishDate.setYear(endDate[1]);
    finishDate.setMonth(months[endDate[0]]);

    //Iterate over months
    while(initialDate<finishDate)
    { 
        if(getNumberOfThursdays(initialDate.getMonth(),initialDate.getYear()))
            counterMonths+=1;

        var newDate = initialDate.setMonth(initialDate.getMonth() + 1);
        initialDate=new Date(newDate);
       
    }
    
    return counterMonths;

}



// Passing the Month and the year, iteraring over the date to find out if there are 5 thrusdays
// returns true if it happens
function getNumberOfThursdays(sMonth, sYear) 
{
            var contador = 0;

            for (var i = 1; i <= 32; i++) 
            {
                var diaDaSemana= new Date();
                diaDaSemana.setYear(sYear);
                diaDaSemana.setMonth(sMonth);
                diaDaSemana.setDate(i);
                if (diaDaSemana.getDay() == 4) 
                    contador+=1;
            }
 
            return contador>=5?true: false;
            
}

console.log(getNumberOfMonths('July 2015 December 2015'));

 